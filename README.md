# Crontab parser

Simple crontab parser written in Rust

# Features

- Validates a cron expression
- Displays future execution time of the task (WIP)
- Take a a sentence in the form "Execute a task []" and build up a cron expression with it (WIP)